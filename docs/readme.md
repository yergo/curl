# Yergo\Curl

## Table of Contents

* [Curl](#curl)
    * [__construct](#__construct)
    * [setContentTranslator](#setcontenttranslator)
    * [send](#send)
    * [configure](#configure)
* [Json](#json)
    * [setResultsToArrays](#setresultstoarrays)
    * [setResultsToObjects](#setresultstoobjects)
    * [encode](#encode)
    * [decode](#decode)
* [Queue](#queue)
    * [__construct](#__construct-1)
    * [add](#add)
    * [queued](#queued)
    * [remove](#remove)
    * [finish](#finish)
* [Request](#request)
    * [id](#id)
    * [resource](#resource)
    * [handle](#handle)
    * [configure](#configure-1)
    * [response](#response)
    * [isFinished](#isfinished)
* [Response](#response-1)
    * [__construct](#__construct-2)
    * [content](#content)
    * [info](#info)
    * [id](#id-1)
* [TranslatingRequest](#translatingrequest)
    * [id](#id-2)
    * [resource](#resource-1)
    * [handle](#handle-1)
    * [configure](#configure-2)
    * [response](#response-2)
    * [isFinished](#isfinished-1)
    * [__construct](#__construct-3)
* [TranslatingResponse](#translatingresponse)
    * [__construct](#__construct-4)
    * [content](#content-1)
    * [info](#info-1)
    * [id](#id-3)

## Curl

Curl Service to provide whole functionality.

Example of usage:
```php
$network = new Curl();
$network->configure([
     CURLOPT_URL => 'example.api',
     CURLOPT_RETURNTRANSFER => true,
     CURLOPT_CUSTOMREQUEST => 'POST',
]);

$request = $network->send();
$response = $request->response();
$content = $response->content();
```

* Full name: \Yergo\Curl


### __construct



```php
Curl::__construct(  )
```







---

### setContentTranslator

Sets up TranslatorInterface to provide functionality of translating requests and responses between formats.

```php
Curl::setContentTranslator( \Yergo\Curl\TranslatorInterface $translator ): $this
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$translator` | **\Yergo\Curl\TranslatorInterface** |  |




---

### send

Sets request to be send by background process.

```php
Curl::send( mixed $content = null, array $configuration = array() ): \Yergo\Curl\RequestInterface
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$content` | **mixed** | null or string or anything parseable by TranslatorInterface delivered previously |
| `$configuration` | **array** | additional CURLOPT_ configuration for this particular request |




---

### configure

Sets up an array of options for any request sent by this instance.

```php
Curl::configure( array $configuration ): \Yergo\Curl
```

```php
$network->configure([
     CURLOPT_URL => 'example.api',
     CURLOPT_RETURNTRANSFER => true,
     CURLOPT_CUSTOMREQUEST => 'POST',
]);
```


**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$configuration` | **array** | configuration of `CURLOPT_* => 'value'` pairs |




---

## Json

TranslatorInterface that enables contents of requests/responses to be serialized/deserialized with JSON.



* Full name: \Yergo\Curl\Translators\Json
* This class implements: \Yergo\Curl\TranslatorInterface


### setResultsToArrays

Switches Translator to deliver translated responses as arrays, instead of objects.

```php
Json::setResultsToArrays(  ): \Yergo\Curl\Translators\Json
```







---

### setResultsToObjects

Switches Translator to deliver translated responses as objects, instead of arrays.

```php
Json::setResultsToObjects(  ): \Yergo\Curl\Translators\Json
```







---

### encode

Encodes delivered content into Json.

```php
Json::encode( mixed $content ): string
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$content` | **mixed** | data to transfer into string content. |


**Return Value:**

content prepared for HTTP request.



---

### decode

Decodes delivered content from JSON.

```php
Json::decode( string $content ): mixed
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$content` | **string** | JSON content from response |


**Return Value:**

object or array after deserialization



---

## Queue

Handles multiple CURL requests



* Full name: \Yergo\Curl\Queue
* This class implements: \Yergo\Curl\QueueInterface


### __construct



```php
Queue::__construct(  )
```







---

### add

Set request to proceed in background.

```php
Queue::add( \Yergo\Curl\RequestInterface $request ): \Yergo\Curl\QueueInterface
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$request` | **\Yergo\Curl\RequestInterface** | request instance to be executed |




---

### queued

Checks if request is already queued.

```php
Queue::queued( \Yergo\Curl\RequestInterface $request ): boolean
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$request` | **\Yergo\Curl\RequestInterface** | Request instance to be checked |




---

### remove

Remover Request from queue.

```php
Queue::remove( \Yergo\Curl\RequestInterface $request ): \Yergo\Curl\QueueInterface
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$request` | **\Yergo\Curl\RequestInterface** | Request instance to be removed |




---

### finish

Awaits for response of particular, already queued Request.

```php
Queue::finish( \Yergo\Curl\RequestInterface $request ): \Yergo\Curl\QueueInterface
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$request` | **\Yergo\Curl\RequestInterface** | Request to be finished. |




---

## Request

Wraps up CURL handler into an configured request.



* Full name: \Yergo\Curl\Request
* This class implements: \Yergo\Curl\RequestInterface


### id

Delivers ID of a request.

```php
Request::id(  ): string
```







---

### resource

Delivers a resource handle itself.

```php
Request::resource(  ): resource
```







---

### handle

Sets up supervising instance.

```php
Request::handle( \Yergo\Curl\QueueInterface $handler ): \Yergo\Curl\RequestInterface
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$handler` | **\Yergo\Curl\QueueInterface** | supervisor instance |




---

### configure

Sets up CURL configuration.

```php
Request::configure( array $configuration ): \Yergo\Curl\RequestInterface
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$configuration` | **array** | array of `CUROPT_* => 'value'` pairs |




---

### response

Delivers instance of a response, that was answered to this request.

```php
Request::response(  ): \Yergo\Curl\ResponseInterface
```







---

### isFinished

Checks if request if already finished.

```php
Request::isFinished(  ): boolean
```







---

## Response

Class Response



* Full name: \Yergo\Curl\Response
* This class implements: \Yergo\Curl\ResponseInterface


### __construct

Response constructor.

```php
Response::__construct( \Yergo\Curl\RequestInterface $request )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$request` | **\Yergo\Curl\RequestInterface** |  |




---

### content



```php
Response::content(  ): string
```







---

### info



```php
Response::info( string $key ): mixed
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$key` | **string** |  |




---

### id



```php
Response::id(  ): string
```







---

## TranslatingRequest

Wraps up CURL handler into an configured request.



* Full name: \Yergo\Curl\TranslatingRequest
* Parent class: \Yergo\Curl\Request


### id

Delivers ID of a request.

```php
TranslatingRequest::id(  ): string
```







---

### resource

Delivers a resource handle itself.

```php
TranslatingRequest::resource(  ): resource
```







---

### handle

Sets up supervising instance.

```php
TranslatingRequest::handle( \Yergo\Curl\QueueInterface $handler ): \Yergo\Curl\RequestInterface
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$handler` | **\Yergo\Curl\QueueInterface** | supervisor instance |




---

### configure

Sets up CURL configuration.

```php
TranslatingRequest::configure( array $configuration ): \Yergo\Curl\RequestInterface
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$configuration` | **array** | array of `CUROPT_* => 'value'` pairs |




---

### response

Delivers instance of a response, that was answered to this request.

```php
TranslatingRequest::response(  ): \Yergo\Curl\ResponseInterface
```







---

### isFinished

Checks if request if already finished.

```php
TranslatingRequest::isFinished(  ): boolean
```







---

### __construct

TranslatingRequest constructor.

```php
TranslatingRequest::__construct( \Yergo\Curl\TranslatorInterface $translator )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$translator` | **\Yergo\Curl\TranslatorInterface** |  |




---

## TranslatingResponse

Class Response



* Full name: \Yergo\Curl\TranslatingResponse
* Parent class: \Yergo\Curl\Response


### __construct

TranslatingResponse constructor.

```php
TranslatingResponse::__construct( \Yergo\Curl\RequestInterface $request, \Yergo\Curl\TranslatorInterface $translator )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$request` | **\Yergo\Curl\RequestInterface** |  |
| `$translator` | **\Yergo\Curl\TranslatorInterface** |  |




---

### content



```php
TranslatingResponse::content(  ): string
```







---

### info



```php
TranslatingResponse::info( string $key ): mixed
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$key` | **string** |  |




---

### id



```php
TranslatingResponse::id(  ): string
```







---



--------
> This document was automatically generated from source code comments on 2017-10-31 using [phpDocumentor](http://www.phpdoc.org/) and [cvuorinen/phpdoc-markdown-public](https://github.com/cvuorinen/phpdoc-markdown-public)
