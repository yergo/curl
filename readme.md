# yergo/curl - lightweight parallel-enabled Curl wrapper

## Idea
After checking out a few implementations of `curl_multi` over public repositories, thought of creating my own. 
Main reason was because all of found solutions were sending requests after queuing all of them, pretty much
disabling most of `curl_multi` feature in favor of ease of implementation.

Library is an attempt to allow sending HTTP requests in parallel as soon, as all required data for them are available.
Especially useful, if you need to complement request frame with some data from database, before queuing it.

**Library does not block as long, as programmer is not trying to get response object.** 

## Installation

To install and use library you should include it in your composer.json file. You can achieve it in two ways, by adding
it by hand within `require` section:


```json
{
    "require": {
        "yergo/curl" : "*"
    }
}
```    
    
or with a command line:

```bash
composer require yergo/curl
```

## Usage

### Basic

For a single request you need to instantiate a Curl object and provide it required configuration. Coniguration method
is compatible with `curl_setopt_array()` options parameter. 

```php
include_once('./vendor/autoload.php');

$server = new Yergo\Curl();
$server->configure([
    CURLOPT_URL => 'https://jsonplaceholder.typicode.com/posts/1',
    CURLOPT_RETURNTRANSFER => true,
]);

$request = $server->send();

echo($request->response()->content());
```

Library is parallel-enabled, which in this case mean, that request is sent without blocking script execution. If it is
important at all, to ensure request is send or to receive a response, the blocking moment is at the call of `response()`
method, because it requires to wait for its internally bound handler to execute completely.

### Lets wrap me for a method

This example receives a few posts from JsonPlaceHolderApi at the same time.

First step would be to wrap Curl class dedicated for a case, like in this Api class

```php
class Api
{
    private $host;
    private $server;

    public function __construct($host)
    {
        $this->host = $host;

        $this->server = new Yergo\Curl();
        $this->server->configure([
            CURLOPT_RETURNTRANSFER => true,
        ]);
    }

    public function get($id)
    {
        $request = $this->server->send(null, [
            CURLOPT_URL => $this->host . '/' . $id
        ]);

        return $request;
    }
}
```

To get 5 different posts from an API at a time of a single request

```php
$postsApi = new Api('https://jsonplaceholder.typicode.com/posts');
$posts = [];

for($id = 1; $id <= 5; $id++) {
    $posts[] = $postsApi->get($id);
}

foreach($posts as $post) {
    var_dump($post->response()->content());
}
```

For further information please take a look at [library documentation](docs/README.md).
