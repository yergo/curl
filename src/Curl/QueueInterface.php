<?php
declare(strict_types=1);

namespace Yergo\Curl;

/**
 * Interface QueueInterface
 * @package Yergo\Curl
 */
interface QueueInterface
{
    /**
     * @param RequestInterface $request
     * @return QueueInterface
     */
    public function add(RequestInterface $request): QueueInterface;

    /**
     * @param RequestInterface $request
     * @return QueueInterface
     */
    public function remove(RequestInterface $request): QueueInterface;

    /**
     * @param RequestInterface $request
     * @return bool
     */
    public function queued(RequestInterface $request): bool;

    /**
     * @param RequestInterface $request
     * @return QueueInterface
     */
    public function finish(RequestInterface $request): QueueInterface;
}