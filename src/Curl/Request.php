<?php
declare(strict_types=1);

namespace Yergo\Curl;

use Yergo\Curl\Exception\AlreadyScheduledRequest;

/**
 * Wraps up CURL handler into an configured request.
 * @package Yergo\Curl
 */
class Request implements RequestInterface
{
    /**
     * Curl handler
     * @var resource
     */
    protected $handle;

    /**
     * Individual, unique ID of a request
     * @var string
     */
    protected $id;

    /**
     * Array of `CURLOPT_*` options to be sent onto handler
     * @var array
     */
    protected $configuration = [];

    /**
     * Supervisor instance.
     * @var QueueInterface
     */
    protected $handler;

    /**
     * Response instance.
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @var bool is request completed.
     */
    protected $finished = false;

    /**
     * Delivers ID of a request.
     * @return string
     */
    public function id(): string
    {
        return $this->setId()->id;
    }

    /**
     * Delivers a resource handle itself.
     * @return resource
     */
    public function resource()
    {
        return $this
            ->createHandle()
            ->configure($this->configuration)
            ->handle;
    }

    /**
     * Sets up supervising instance.
     *
     * @param QueueInterface $handler supervisor instance
     * @return RequestInterface
     * @throws AlreadyScheduledRequest
     */
    public function handle(QueueInterface $handler): RequestInterface
    {
        if (!$this->handler instanceof QueueInterface) {
            $this->handler = $handler;
        } else {
            throw new AlreadyScheduledRequest('Request already has its handler set up');
        }

        return $this;
    }

    /**
     * Sets up CURL configuration.
     *
     * @param array $configuration array of `CUROPT_* => 'value'` pairs
     * @return RequestInterface
     */
    public function configure(array $configuration): RequestInterface
    {
        $this->configuration = $configuration;
        if (is_resource($this->handle)) {
            curl_setopt_array($this->handle, $configuration);
        }

        return $this;
    }

    /**
     * Delivers instance of a response, that was answered to this request.
     * @return ResponseInterface
     */
    public function response(): ResponseInterface
    {
        if (!$this->response instanceof ResponseInterface) {
            if (!$this->isFinished() && $this->handler->queued($this)) {
                $this->handler->finish($this);
            }

            $this->response = new Response($this);
            $this->finished = true;
        }

        return $this->response;
    }

    /**
     * @return Request
     */
    private function setId(): self
    {
        if (empty($this->id)) {
            $this->id = uniqid();
        }

        return $this;
    }

    /**
     * @return Request
     */
    private function createHandle(): self
    {
        if (!is_resource($this->handle)) {
            $this->handle = curl_init();
        }

        return $this;
    }

    /**
     * Checks if request if already finished.
     *
     * @return bool
     */
    public function isFinished(): bool
    {
        return $this->finished;
    }
}