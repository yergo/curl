<?php
declare(strict_types=1);

namespace Yergo\Curl;

use Yergo\Curl\Exception;

/**
 * Handles multiple CURL requests
 * @package Yergo\Curl
 */
class Queue implements QueueInterface
{
    /**
     * curl_multi resource handler
     * @var resource
     */
    private $handler;

    /**
     * Number of running processes
     * @var int
     */
    private $running = 0;

    /**
     * Storage of request instances
     * @var array
     */
    private $requestsQueue = [];

    public function __construct()
    {
        $this->handler = curl_multi_init();
    }

    /**
     * Set request to proceed in background.
     *
     * @param RequestInterface $request request instance to be executed
     * @return QueueInterface
     * @throws Exception\AlreadyScheduledRequest
     */
    public function add(RequestInterface $request): QueueInterface
    {
        if ($this->queued($request)) {
            throw new Exception\AlreadyScheduledRequest('Request is already scheduled.');
        }

        $this->requestsQueue[$request->id()] = $request;
        $request->handle($this);

        return $this->addToHandle($request->resource());
    }

    /**
     * Checks if request is already queued.
     *
     * @param RequestInterface $request Request instance to be checked
     * @return bool
     */
    public function queued(RequestInterface $request): bool
    {
        return array_key_exists($request->id(), $this->requestsQueue);
    }

    /**
     * @param $resource
     * @return QueueInterface
     * @throws Exception\ImproperCurlStatus
     */
    private function addToHandle($resource): QueueInterface
    {
        $status = curl_multi_add_handle($this->handler, $resource);
        if ($status !== CURLM_OK) {
            throw new Exception\ImproperCurlStatus('Error occurred when creating curl request.', $status);
        }

        $status = curl_multi_exec($this->handler, $this->running);
        if ($status !== CURLM_OK) {
            throw new Exception\ImproperCurlStatus('Error occurred while proceeding background jobs.', $status);
        }

        return $this;
    }

    /**
     * Remover Request from queue.
     *
     * @param RequestInterface $request Request instance to be removed
     * @return QueueInterface
     * @throws Exception\UnscheduledRequest
     */
    public function remove(RequestInterface $request): QueueInterface
    {
        unset($this->requestsQueue[$request->id()]);
        return $this->removeFromHandle($request);
    }

    /**
     * Awaits for response of particular, already queued Request.
     *
     * @param RequestInterface $request Request to be finished.
     * @return QueueInterface
     * @throws Exception\UnscheduledRequest
     */
    public function finish(RequestInterface $request): QueueInterface
    {
        if (!$this->queued($request)) {
            throw new Exception\UnscheduledRequest('Tried to await result of an unscheduled request');
        }

        while (
            !$request->isFinished() ||
            $this->running > 0
        ) {
            while (
                false === ($info = curl_multi_info_read($this->handler)) ||
                $info['msg'] !== CURLMSG_DONE
            ) {
                curl_multi_select($this->handler);
                curl_multi_exec($this->handler, $this->running);
            }

            /**
             * @var $queuedRequest RequestInterface
             */
            foreach ($this->requestsQueue as $id => $queuedRequest) {
                if ($info['handle'] !== $queuedRequest->resource()) {
                    continue;
                }

                $this->finalize($queuedRequest);
                break;
            }
        }

        return $this;
    }

    /**
     * @param RequestInterface $request
     * @return Queue
     */
    private function finalize(RequestInterface $request): self
    {
        unset($this->requestsQueue[$request->id()]);
        $request->response();

        return $this->remove($request);
    }

    /**
     * @param RequestInterface $request
     * @return Queue
     * @throws Exception\UnscheduledRequest
     */
    private function removeFromHandle(RequestInterface $request): self
    {
        curl_multi_remove_handle($this->handler, $request->resource());
        return $this;
    }
}
