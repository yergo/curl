<?php
declare(strict_types=1);

namespace Yergo\Curl;

/**
 * Class RequestInterface
 * @package Yergo\Curl
 */
interface RequestInterface
{
    /**
     * Get unique request id.
     * @return string
     */
    public function id(): string;

    /**
     * Retrieve a finally configured resource handler
     * @return resource
     */
    public function resource();

    /**
     * @return ResponseInterface
     */
    public function response(): ResponseInterface;

    /**
     * @param QueueInterface $handler
     * @return RequestInterface
     */
    public function handle(QueueInterface $handler);

    /**
     * @return bool
     */
    public function isFinished(): bool;

    /**
     * @param array $configuration
     * @return RequestInterface
     */
    public function configure(array $configuration): RequestInterface;
}