<?php
declare(strict_types=1);

namespace Yergo\Curl\Translators;

use Yergo\Curl\Exception;
use Yergo\Curl\TranslatorInterface;

/**
 * TranslatorInterface that enables contents of requests/responses to be serialized/deserialized with JSON.
 *
 * @package Yergo\Curl\Translators
 */
class Json implements TranslatorInterface
{
    /**
     * Should we deliver decoded results as arrays
     * @var bool
     */
    protected $resultsAsArray = false;

    /**
     * Switches Translator to deliver translated responses as arrays, instead of objects.
     *
     * @return Json
     */
    public function setResultsToArrays(): self
    {
        $this->resultsAsArray = true;
        return $this;
    }

    /**
     * Switches Translator to deliver translated responses as objects, instead of arrays.
     *
     * @return Json
     */
    public function setResultsToObjects(): self
    {
        $this->resultsAsArray = false;
        return $this;
    }

    /**
     * Encodes delivered content into Json.
     *
     * @param mixed $content data to transfer into string content.
     * @return string content prepared for HTTP request.
     * @throws Exception\TranslationFailed
     */
    public function encode($content): string
    {
        $encoded = json_encode($content);
        $error = json_last_error_msg();

        if ($encoded === false && !is_null($error)) {
            throw new Exception\TranslationFailed($error);
        }

        return $encoded;
    }

    /**
     * Decodes delivered content from JSON.
     *
     * @param string $content JSON content from response
     * @return mixed object or array after deserialization
     * @throws Exception\TranslationFailed
     */
    public function decode(string $content)
    {
        $decoded = json_decode($content, $this->resultsAsArray);
        $error = json_last_error_msg();

        if (is_null($decoded) && !is_null($error)) {
            throw new Exception\TranslationFailed($error);
        }

        return $decoded;
    }
}