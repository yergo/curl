<?php
declare(strict_types=1);

namespace Yergo\Curl;

use Yergo\Curl\Exception;

class TranslatingRequest extends Request
{
    protected $translator;

    /**
     * TranslatingRequest constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param array $configuration
     * @return RequestInterface
     */
    public function configure(array $configuration): RequestInterface
    {
        $this->configuration = $configuration;
        if (is_resource($this->handle)) {
            if (
                array_key_exists(CURLOPT_POSTFIELDS, $configuration) &&
                !empty($configuration[CURLOPT_POSTFIELDS])
            ) {
                $content = $this->encode($configuration[CURLOPT_POSTFIELDS]);

                $configuration[CURLOPT_POSTFIELDS] = $content;
                $configuration[CURLOPT_HTTPHEADER]['content-length'] =
                    'Content-Length: ' . strlen($configuration[CURLOPT_POSTFIELDS]);
            }

            curl_setopt_array($this->handle, $configuration);
        }

        return $this;
    }

    /**
     * @return ResponseInterface
     */
    public function response(): ResponseInterface
    {
        if (!$this->response instanceof ResponseInterface) {
            if (!$this->isFinished() && $this->handler->queued($this)) {
                $this->handler->finish($this);
            }

            $this->response = new TranslatingResponse($this, $this->translator);
            $this->finished = true;
        }

        return $this->response;
    }

    /**
     * @param $content
     * @return string
     * @throws Exception\TranslationFailed
     */
    private function encode($content)
    {
        return $this->translator->encode($content);
    }
}
