<?php
declare(strict_types=1);

namespace Yergo\Curl;

/**
 * Class Response
 * @package Yergo\Curl
 */
class Response implements ResponseInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $content;

    /**
     * @var array
     */
    protected $info;

    /**
     * Response constructor.
     * @param RequestInterface $request
     */
    public function __construct(RequestInterface $request)
    {
        $resource = $request->resource();

        $this->id = $request->id();
        $this->info = curl_getinfo($resource);
        $this->content = curl_multi_getcontent($resource);
    }

    /**
     * @return string
     */
    public function content()
    {
        return $this->content;
    }

    /**
     * @param string $key
     * @return mixed
     * @throws \Error
     */
    public function info(string $key)
    {
        if (array_key_exists($key, $this->info)) {
            return $this->info[$key];
        }

        throw new \Error('Unknown Curl info option', E_USER_WARNING);
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}