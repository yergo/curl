<?php
declare(strict_types=1);

namespace Yergo\Curl;

/**
 * Interface TranslatorInterface
 * @package Yergo\Curl
 */
interface TranslatorInterface
{
    /**
     * @param mixed $content
     * @return string
     */
    public function encode($content): string;

    /**
     * @param string $content
     * @return mixed
     */
    public function decode(string $content);
}