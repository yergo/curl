<?php
declare(strict_types=1);

namespace Yergo\Curl;

/**
 * Class ResponseInterface
 * @package Yergo\Curl
 */
interface ResponseInterface
{
    /**
     * @return string
     */
    public function content();

    /**
     * @param string $key
     * @return mixed
     */
    public function info(string $key);

    /**
     * @return string
     */
    public function id(): string;
}