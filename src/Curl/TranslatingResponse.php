<?php
declare(strict_types=1);

namespace Yergo\Curl;

class TranslatingResponse extends Response
{
    /**
     * TranslatingResponse constructor.
     * @param RequestInterface $request
     * @param TranslatorInterface $translator
     */
    public function __construct(RequestInterface $request, TranslatorInterface $translator)
    {
        parent::__construct($request);
        $this->content = $translator->decode($this->content);
    }
}