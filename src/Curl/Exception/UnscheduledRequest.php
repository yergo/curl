<?php
declare(strict_types=1);

namespace Yergo\Curl\Exception;

/**
 * Class UnscheduledRequest
 * @package Yergo\Curl\Exception
 */
class UnscheduledRequest extends \Exception
{

}