<?php
declare(strict_types=1);

namespace Yergo\Curl\Exception;

/**
 * Class TranslationFailed
 * @package Yergo\Curl\Exception
 */
class TranslationFailed extends \Exception
{

}