<?php
declare(strict_types=1);

namespace Yergo\Curl\Exception;

/**
 * Class ImproperCurlStatus
 * @package Yergo\Curl\Exception
 */
class ImproperCurlStatus extends \Exception
{

}