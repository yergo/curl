<?php
declare(strict_types=1);

namespace Yergo\Curl\Exception;

/**
 * Class AlreadyScheduledRequest
 * @package Yergo\Curl\Exception
 */
class AlreadyScheduledRequest extends \Exception
{

}