<?php
declare(strict_types=1);

namespace Yergo;

use Yergo\Curl\Queue;
use Yergo\Curl\QueueInterface;
use Yergo\Curl\Request;
use Yergo\Curl\RequestInterface;
use Yergo\Curl\TranslatingRequest;
use Yergo\Curl\TranslatorInterface;

/**
 * Curl Service to provide whole functionality.
 *
 * Example of usage:
 * ```php
 * $network = new Curl();
 * $network->configure([
 *      CURLOPT_URL => 'example.api',
 *      CURLOPT_RETURNTRANSFER => true,
 *      CURLOPT_CUSTOMREQUEST => 'POST',
 * ]);
 *
 * $request = $network->send();
 * $response = $request->response();
 * $content = $response->content();
 * ```
 *
 * @package Yergo
 */
class Curl
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var QueueInterface
     */
    protected $queue;

    /**
     * @var array
     */
    protected $configuration;

    public function __construct()
    {
        $this->configuration = [];
        $this->queue = new Queue();
    }

    /**
     * Sets up TranslatorInterface to provide functionality of translating requests and responses between formats.
     *
     * @param TranslatorInterface $translator
     * @return $this
     */
    public function setContentTranslator(TranslatorInterface $translator): self
    {
        $this->translator = $translator;
        return $this;
    }

    /**
     * Sets request to be send by background process.
     *
     * @param mixed $content null or string or anything parseable by TranslatorInterface delivered previously
     * @param array $configuration additional CURLOPT_ configuration for this particular request
     * @return RequestInterface
     */
    public function send($content = null, array $configuration = []): RequestInterface
    {
        $request = $this->createRequestObject();
        $request->configure($this->buildRequestConfig($content, $configuration));
        $this->queue->add($request);

        return $request;
    }

    /**
     * Sets up an array of options for any request sent by this instance.
     *
     * ```php
     * $network->configure([
     *      CURLOPT_URL => 'example.api',
     *      CURLOPT_RETURNTRANSFER => true,
     *      CURLOPT_CUSTOMREQUEST => 'POST',
     * ]);
     * ```
     * @param array $configuration configuration of `CURLOPT_* => 'value'` pairs
     * @return Curl
     */
    public function configure(array $configuration): self
    {
        $this->configuration = $configuration;
        return $this;
    }

    /**
     * @param $content
     * @param array $configuration
     * @return array
     */
    private function buildRequestConfig($content, array $configuration = []): array
    {
        $result = $this->configuration;

        if (!array_key_exists(CURLOPT_HTTPHEADER, $configuration)) {
            $configuration[CURLOPT_HTTPHEADER] = [];
        }

        if (!is_null($content)) {
            $configuration[CURLOPT_POSTFIELDS] = $content;
            if (is_string($content)) {
                $configuration[CURLOPT_HTTPHEADER][] = 'Content-Length: ' . strlen($content);
            }
        }

        $configuration[CURLOPT_HTTPHEADER] = $this->prepareCurloptHttpHeaders($configuration[CURLOPT_HTTPHEADER]);

        foreach ($configuration as $key => $value) {
            $result[$key] = $value;
        }

        return $result;
    }

    /**
     * @param $headers
     * @return array
     */
    protected function prepareCurloptHttpHeaders(array $headers): array
    {
        $httpHeaders = [];
        $configuration[CURLOPT_HTTPHEADER] = array_filter($headers, 'trim');

        foreach ($headers as $value) {
            $position = strpos($value, ':');
            if ($position === false || $position < 1) {
                continue;
            }

            $key = trim(strtolower(substr($value, 0, $position-1 ?: '')));
            $httpHeaders[$key] = $value;
        }

        return $httpHeaders;
    }

    /**
     * @return RequestInterface
     */
    protected function createRequestObject(): RequestInterface
    {
        if ($this->translator instanceof TranslatorInterface) {
            return new TranslatingRequest($this->translator);
        }

        return new Request();
    }
}