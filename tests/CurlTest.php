<?php
declare(strict_types=1);

namespace Yergo\Tests;

use Yergo\Curl;
use Yergo\Tests\Curl\TestCase;

class CurlTest extends TestCase
{
    /**
     * @var Curl
     */
    protected $network;

    protected function setUp()
    {
        parent::setUp();
        $this->network = new Curl();
    }

    /**
     * @return Curl
     */
    public function testCanConfigureGlobalSerialization()
    {
        $this->network->setContentTranslator(new Curl\Translators\Json());
        $this->assertTrue(true);

        return $this->network;
    }

    public function testCanConfigureGlobalConfiguration()
    {
        $this->network->configure([
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => 'POST',
        ]);

        $this->assertTrue(true);
    }

    public function testCanSendData()
    {
        $this->network->configure([
            CURLOPT_URL => \ServerPool::getUrl() . '?_t=0',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => 'POST',
        ]);

        $ping1 = $this->network->send();

        $this->assertInstanceOf(Curl\Request::class, $ping1);
        $this->assertEquals('OK: 0', trim($ping1->response()->content()));

        return $this->network;
    }

    /**
     * @depends testCanSendData
     * @param $network Curl
     */
    public function testCanSendDataWithAdditionalConfiguration($network)
    {
        $ping2 = $network->send('{"return":1234}', [
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'header w/o key to be ignored',
                ':header w empty key to be ignored',
            ]
        ]);

        $this->assertInstanceOf(Curl\Request::class, $ping2);
        $this->assertEquals('1234', intval($ping2->response()->content()));
    }

    /**
     * @depends testCanConfigureGlobalSerialization
     * @param $network Curl
     * @return Curl
     */
    public function testCanSendDataWithCustomSerialization($network)
    {
        $network->configure([
            CURLOPT_URL => \ServerPool::getUrl() . '?_t=0',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => 'POST',
        ]);

        $random1 = mt_rand(0, 10000);
        $random2 = mt_rand(10000, 20000);
        $ping1 = $network->send(['return' => $random1]);

        $testObj = new \stdClass();
        $testObj->return = $random2;

        $ping2 = $network->send($testObj);

        $this->assertInstanceOf(Curl\TranslatingRequest::class, $ping1, 'Inconsistency converting array into TranslatingRequest');
        $this->assertInstanceOf(Curl\TranslatingRequest::class, $ping2, 'Inconsistency converting stdClass into TranslatingRequest');

        $this->assertEquals($random1, $ping1->response()->content());
        $this->assertEquals($random2, $ping2->response()->content());

        return $network;
    }

    /**
     * @depends testCanSendDataWithCustomSerialization
     * @param $network Curl
     * @return Curl\RequestInterface
     */
    public function testSendsEmptyContentOnSerializationIfSendDataIsNull($network)
    {
        $ping3 = $network->send(null);
        $this->assertFalse($ping3->isFinished(), 'Request should not be finished directly after sending');

        return $ping3;
    }

    /**
     * @expectedException \Yergo\Curl\Exception\TranslationFailed
     * Echo test script wont answer with proper json, so we expect syntax-error
     * while sending not-json data.
     *
     * @depends testSendsEmptyContentOnSerializationIfSendDataIsNull
     * @param $request
     */
    public function testWillThrowExceptionWhileJsonErrorOnReceivedContent($request)
    {
        $request->response()->content();
    }
}
