<?php
declare(strict_types=1);

namespace Yergo\Tests;

use Yergo\Curl;
use Yergo\Tests\Curl\TestCase;

class QueueTest extends TestCase
{
    /**
     * @var Curl\Queue
     */
    protected $queue;

    /**
     * @var Curl
     */
    protected $service;

    protected function setUp()
    {
        parent::setUp();
        $this->service = new Curl();
        $this->queue = new Curl\Queue();
    }

    public function testServiceInstanceShouldBeCurl()
    {
        $this->assertEquals(Curl::class, get_class($this->service));
    }

    public function testQueueInstanceShouldBeQueue()
    {
        $this->assertEquals(Curl\Queue::class, get_class($this->queue));
    }

    /**
     * @expectedException \Yergo\Curl\Exception\AlreadyScheduledRequest
     */
    public function testSameRequestHandledTwiceShouldFail()
    {
        $request = (new Curl\Request())->configure($this->getRequest(0));
        $this->queue->add($request);
        $request->handle($this->queue);
    }

    /**
     * @expectedException \Yergo\Curl\Exception\AlreadyScheduledRequest
     */
    public function testSameRequestAddedTwiceShouldFail()
    {
        $request = (new Curl\Request())->configure($this->getRequest(0));
        $this->queue->add($request);
        $this->queue->add($request);
    }

    /**
     * @expectedException \Yergo\Curl\Exception\AlreadyScheduledRequest
     */
    public function testSameRequestAddedSecondTimeEvenAfterFinishShouldFail()
    {
        $request = (new Curl\Request())->configure($this->getRequest(0));
        $this->queue->add($request);
        $this->queue->finish($request);
        $this->queue->add($request);
    }

    public function testRequestRemovalWhenNotScheduledShouldNotFail()
    {
        $request = new Curl\Request();
        $this->queue->remove($request);

        $this->assertTrue(true);
    }

    /**
     * @expectedException \Yergo\Curl\Exception\UnscheduledRequest
     */
    public function testTryingToFinishNotAddedRequestShouldFail()
    {
        $request = new Curl\Request();
        $this->queue->finish($request);
    }

    /**
     * @expectedException \Error
     */
    public function testTryingToGetResponseOfNotAddedRequestShouldFail()
    {
        $request = new Curl\Request();
        $request->response();
    }

    public function testSingleRequestShouldDeliverContent()
    {
        $conf = $this->getRequest(1);
        $random = $conf[CURLOPT_PRIVATE];

        $request = (new Curl\Request())->configure($conf);

        $this->queue->add($request);
        $this->queue->finish($request);

        $this->assertEquals($random, $request->response()->content());
    }

    public function testMultipleRequestsShouldDeliverContentInTimeOfTheLongestOfThem()
    {
        $start = microtime(true);
        $requests = [];
        $times = [4, 3, 2, 2, 1, 1];

        foreach ($times as $t) {
            $requestConf = $this->getRequest($t);
            $requests[] = [
                'expected' => $requestConf[CURLOPT_PRIVATE],
                'object' => (new Curl\Request())->configure($requestConf)
            ];

            $this->queue->add(end($requests)['object']);
            usleep(500000);
        }

        foreach ($requests as $request) {
            $this->assertEquals($request['expected'], $request['object']->response()->content());
        }

        $this->assertTrue(
            max($times)+1 > microtime(true)-$start,
            'Finalizing all requests took too long'
        );

        return reset($requests)['object'];
    }

    /**
     * @depends testMultipleRequestsShouldDeliverContentInTimeOfTheLongestOfThem
     * @param Curl\RequestInterface $request
     */
    public function testIdOfResponseShouldMatchIdOfRequest(Curl\RequestInterface $request)
    {
        $this->assertEquals($request->id(), $request->response()->id());
    }

    /**
     * @depends testMultipleRequestsShouldDeliverContentInTimeOfTheLongestOfThem
     * @param Curl\RequestInterface $request
     * @return Curl\ResponseInterface
     */
    public function testCurlInfoResultKeyShouldWorkForRequestsInfo(Curl\RequestInterface $request)
    {
        $this->assertGreaterThan(0, $request->response()->info('total_time'));
        return $request->response();
    }

    /**
     * @depends testCurlInfoResultKeyShouldWorkForRequestsInfo
     * @expectedException \Error
     * @param Curl\ResponseInterface $response
     */
    public function testResponseInfoOnUndefinedCurlResultKeyShouldFailWithError(Curl\ResponseInterface $response)
    {
        $this->assertNull($response->info('undefined key'));
    }

    private static function getRequest($t)
    {
        $random = mt_rand(10000, 20000);
        $content = json_encode([
            'return' => $random
        ]);

        $conf = [
            CURLOPT_URL => \ServerPool::getUrl() . '?_t=' . $t,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $content,
            CURLOPT_PRIVATE => $random,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($content),
            ]
        ];

        return $conf;
    }
}
