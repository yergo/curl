<?php
$time = isset($_GET['_t']) && $_GET['_t'] <= 10 ? $_GET['_t'] : 0;
sleep($time);

$json = file_get_contents('php://input') ?: (isset($_REQUEST['json']) ? $_REQUEST['json'] : false);

if (($request = json_decode($json, 1))) {
    if (isset($request['return'])) {
        echo($request['return']);
        die();
    }
}

echo('OK: ' . $time . PHP_EOL);
