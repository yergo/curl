<?php
namespace Yergo\Tests\Curl\Translators;

use Yergo\Curl\Translators\Json;
use Yergo\Tests\Curl\TestCase;

class JsonTest extends TestCase
{

    /**
     * @var Json
     */
    protected $translator;

    protected function setUp()
    {
        parent::setUp();
        $this->translator = new Json();
    }

    public function testIsEncodingArrayToJson()
    {
        $input = [
            'a' => "A",
            0 => null,
            'b' => false
        ];
        $expected = json_encode($input);

        $this->assertEquals($expected, $this->translator->encode($input));
    }

    public function testIsEncodingStringToJsSafeString()
    {
        $input = 'Big brown fox doing something "wierd".';
        $expected = '"Big brown fox doing something \"wierd\"."';
        $this->assertEquals($expected, $this->translator->encode($input));
    }

    public function testIsDecodingJsonToArray()
    {
        $expected = [
            'a' => "A",
            0 => null,
            'b' => false
        ];
        $input = json_encode($expected);
        $this->translator->setResultsToArrays();
        $this->assertEquals($expected, $this->translator->decode($input));
    }

    public function testIsDecodingJsonToObject()
    {
        $input = json_encode([
            'a' => "A",
            0 => null,
            'b' => false
        ]);

        $zero = 0;
        $expected = new \stdClass();
        $expected->a = "A";
        $expected->{$zero} = null;
        $expected->b = false;
        $this->translator->setResultsToObjects();

        $this->assertEquals($expected, $this->translator->decode($input));
    }
}
