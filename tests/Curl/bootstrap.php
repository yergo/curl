<?php

include __DIR__ . "/../../vendor/autoload.php";

/**
 * Server instance to enable testing requests.
 *
 * Class Server
 */
class Server
{
    static $nextPort = WEB_SERVER_PORT_RANGE_START;

    protected $port;
    protected $pid;

    /**
     * Server constructor.
     */
    public function __construct()
    {
        $this->port = self::$nextPort++;
        $this->start();
    }

    public function __destruct()
    {
        $this->stop();
    }

    public function getUrl()
    {
        $host = sprintf('http://%s:%d/', WEB_SERVER_HOST, $this->port);
        return $host;
    }

    public function start()
    {
        $command = sprintf(
            'php -S %s:%d -t %s > /dev/null 2>&1 & echo $!',
            WEB_SERVER_HOST,
            $this->port,
            WEB_SERVER_ROOT
        );

        exec($command, $output);
        $this->pid = (int) $output[0];
    }

    public function stop()
    {
        exec('kill ' . $this->pid);
    }
}

/**
 * Class ServerPool
 *
 * As of PHP built-in server is single-threaded, we have to spawn
 * a few instances to test our class for parallel requests.
 */
class ServerPool
{
    protected static $limit;
    protected static $pool = [];
    protected static $position = 0;

    protected static $running = false;

    public static function start($size = 10)
    {
        if (self::$running === true) return;

        self::$limit = $size;

        for($i = 0, $n = self::$limit; $i < $n; $i++) {
            $server = new Server();
            self:: $pool[] = $server;
        }

        return;
    }

    public static function getUrl()
    {
        $result = self::$pool[self::$position]->getUrl();
        self::incrementPosition();

        return $result;
    }

    protected static function incrementPosition()
    {
        self::$position++;
        if (self::$position >= count(self::$pool)) {
            self::$position = 0;
        }
    }
}

echo 'Warming up local server instances for parallel tests.' . PHP_EOL;
ServerPool::start(10);
sleep(1);
